﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace etherscan_price_updater
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Token price updater started");
            Timer timer = new Timer();
            timer.Interval = 300000;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();

            Console.ReadLine();
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //Get list of tokens
            Console.WriteLine("Get all tokens in database");
            List<Price> symbols = GetTokenList();

            foreach (var item in symbols)
            {
                // Initilization.  
                List<KeyValuePair<string, string>> allIputParams = new List<KeyValuePair<string, string>>();
                string requestParams = string.Empty;

                // Converting Request Params to Key Value Pair.  
                allIputParams.Add(new KeyValuePair<string, string>("fsym", item.symbol));
                allIputParams.Add(new KeyValuePair<string, string>("tsyms", "USD"));

                // URL Request Query parameters.  
                requestParams = new FormUrlEncodedContent(allIputParams).ReadAsStringAsync().Result;

                // Call REST Web API with parameters.  
                Console.WriteLine("Calling API to get price for " + item.symbol);
                item.price = CallAPI(requestParams).Result;

                //Update price in db
                UpdateTokenPrice(item);
                Console.WriteLine("Token price updated");
            }
        }

        private static List<Price> GetTokenList()
        {
            List<Price> tokenDetails = new List<Price>();
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("SELECT id, symbol from Token", con))
                {
                    con.Open();
                    using (MySqlDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            Price tokenData = new Price();
                            tokenData.id = sdr["id"].ToString();
                            tokenData.symbol = sdr["symbol"].ToString();
                            tokenDetails.Add(tokenData);
                        }
                    }
                    con.Close();
                }
            }

            return tokenDetails;
        }

        private static async Task<string> CallAPI(string requestParams)
        {
            string price = "0.00";

            // HTTP GET.  
            using (var client = new HttpClient())
            {
                // Setting Base address.  
                client.BaseAddress = new Uri("https://min-api.cryptocompare.com/data/");

                // Setting content type.  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // Initialization.  
                HttpResponseMessage response = new HttpResponseMessage();

                // HTTP GET  
                response = await client.GetAsync("price?" + requestParams).ConfigureAwait(false);

                // Verification  
                if (response.IsSuccessStatusCode)
                {
                    // Reading Response.  
                    string result = response.Content.ReadAsStringAsync().Result;
                    var res = (JObject)JsonConvert.DeserializeObject(result);
                    price = res.Value<string>("USD");
                }
            }

            return price;
        }

        public static void UpdateTokenPrice(Price token)
        {
            string constr = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;

            using (MySqlConnection con = new MySqlConnection(constr))
            {
                using (MySqlCommand cmd = new MySqlCommand("UPDATE token SET price=@price WHERE id=@id"))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@id", token.id);

                    if (!string.IsNullOrEmpty(token.price))
                        cmd.Parameters.AddWithValue("@price", token.price);
                    else
                        cmd.Parameters.AddWithValue("@price", 0.00);

                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}
