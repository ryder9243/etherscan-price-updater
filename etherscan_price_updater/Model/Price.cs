﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace etherscan_price_updater
{
    public class Price
    {
        public string id { get; set; }
        public string symbol { get; set; }
        public string price { get; set; }
    }
}
